import React from 'react'
import { render } from 'react-dom'
import App from './modules/App'
import { createBrowserHistory } from 'history';
import { createCognissStore } from './modules/store'
import 'font-awesome/css/font-awesome.css'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import './index.css'

const target = document.querySelector('#root')
const publicUrl = process.env.PUBLIC_URL
const history = createBrowserHistory({basename:publicUrl})
const store = createCognissStore(history)

render(
  <App history={history} store={store} />,
  target
)
