import React, { useState } from "react";
import styled from "styled-components";
import Modal from "../common/components/Modal";
import ContentDropdown from "../common/components/ContentDropdown";
import ContentTabs from "../common/components/ContentTabs";
import GenericButton from "../common/components/GenericButton";

const TermsPrivacyPolicy = ({ modalState, setModalState, history }) => {
  const [conditionsAccepted, setConditionsAccepted] = useState(false);

  const ModalHeader = styled.div`
    width: 100%;
    min-height: 20px;
    background-color: ${props => props.theme.colors.offWhite};
    padding: 20px;
    font-size: 18px;
    font-weight: 600;
    margin-bottom: 24px;
  `;

  const ModalBody = styled.div`
    width: 100%;
    height: 300px;
    background-color: ${props => props.theme.colors.white};
    padding: 0 24px;
    overflow: scroll;
    border-bottom: 1px solid ${props => props.theme.colors.lightGrey};
    margin-bottom: 12px;
  `;

  const Section = styled.div`
    padding-bottom: 12px;
  `;

  const Checkbox = styled.input.attrs({ type: "checkbox" })`
    transform: scale(1.5);
    -ms-transform: scale(1.5);
    -webkit-transform: scale(1.5);
    margin: 0;
    padding: 10px;
  `;

  const CheckboxText = styled.span`
    margin-left: 16px;
  `;

  const ButtonsWrapper = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    padding: 16px;
  `;

  const modalStyles = {
    content: {
      top: "50%",
      left: "50%",
      right: "auto",
      bottom: "auto",
      transform: "translate(-50%, -50%)",
      padding: "0",
      width: "70%",
      textAlign: "center"
    }
  };

  const closeModal = action => {
    setModalState(false);
    setConditionsAccepted(false);

    if (action === "accepted") {
      history.push("/register");
    }
  };

  return (
    <Modal
      modalState={modalState}
      styles={modalStyles}
      label="Terms and Privacy Policy"
    >
      <ModalHeader>Terms &amp; Privacy Policy</ModalHeader>
      <ModalBody>
        <ContentDropdown heading="Statement of Intent">
          <p>
            However, scrolling inside a modal will lead to body scrolling which
            is kind of annoying. I searched on the internet but didn’t find a
            perfect
          </p>
        </ContentDropdown>
        <ContentTabs
          tabs={[
            {
              heading: "Terms of Use",
              content: (
                <React.Fragment>
                  <Section>
                    <b>What personal information we collect</b>
                    <p>
                      Depending on how you engage with Cogniss, we collect
                      different types of information from you.
                    </p>
                  </Section>
                  <Section>
                    <b>Account Profile and Information</b>
                    <p>
                      I know it's plenty of tutoriales showing that syntax. But
                      there's also a considerable number of blog posts warning
                      about why that's not a good idea. Basically, you are
                      creating a new function on each render.
                    </p>
                  </Section>
                  <Section>
                    <b>Information you provide us</b>
                    <p>
                      The above example will apply the fade transition globally,
                      affecting all modals whose afterOpen and beforeClose
                      classes have not been set via the className prop.
                    </p>
                  </Section>
                  <Section>
                    <b>What personal information we collect</b>
                    <p>
                      Depending on how you engage with Cogniss, we collect
                      different types of information from you.
                    </p>
                  </Section>
                </React.Fragment>
              )
            },
            {
              heading: "Privacy Policy",
              content: (
                <React.Fragment>
                  <Section>
                    <b>Information you provide us</b>
                    <p>
                      The above example will apply the fade transition globally,
                      affecting all modals whose afterOpen and beforeClose
                      classes have not been set via the className prop.
                    </p>
                  </Section>
                  <Section>
                    <b>What personal information we collect</b>
                    <p>
                      Depending on how you engage with Cogniss, we collect
                      different types of information from you.
                    </p>
                  </Section>
                  <Section>
                    <b>Account Profile and Information</b>
                    <p>
                      I know it's plenty of tutoriales showing that syntax. But
                      there's also a considerable number of blog posts warning
                      about why that's not a good idea. Basically, you are
                      creating a new function on each render.
                    </p>
                  </Section>
                  <Section>
                    <b>What personal information we collect</b>
                    <p>
                      Depending on how you engage with Cogniss, we collect
                      different types of information from you.
                    </p>
                  </Section>
                </React.Fragment>
              )
            }
          ]}
        />
      </ModalBody>
      <Checkbox
        checked={conditionsAccepted}
        onChange={() => {
          setConditionsAccepted(!conditionsAccepted);
        }}
      />
      <CheckboxText>I have read and agreed to personal conditions</CheckboxText>
      <ButtonsWrapper>
        <GenericButton
          onClick={() => {
            closeModal("cancelled");
          }}
          text="Cancel"
          type="inverse"
        />
        <GenericButton
          onClick={() => {
            closeModal("accepted");
          }}
          text="Accept"
          disabled={!conditionsAccepted}
        />
      </ButtonsWrapper>
    </Modal>
  );
};
export default TermsPrivacyPolicy;
