import React from 'react'
import styled from 'styled-components'
import Screen from './Screen'
const Wrapper = styled(Screen)`
  text-align:center;
  width:100%;
  .header{
    background-color:${props => props.theme.colors.dark};
    color:${props => props.theme.colors.white};
    padding: 40px 15px;
    .header-container {
      margin: auto;
      max-width: 1090px;
    }
    .close{
      color: white;
      opacity: 1;
      text-shadow: none;
      font-size: 30px;
      margin-left: -20px;
      &:hover, &:active, &:focus{
        color:${props => props.theme.colors.primary};
      }
    }
  }
  .body{
    background-color:${props => props.theme.colors.offWhite};
    padding: 20px 15px 20px 15px;
    display: flex;
    flex: 1 0;
    flex-direction: column;
    margin: auto;
    @media (min-width: 768px) {
      padding: 20px 30px 20px 30px;
      max-width: 860px;
    }
  }
`;
export default ({children, header, className, fullscreen})=>{
  return (<Wrapper className={className} fullscreen={fullscreen}>
    {header && <div className="header" children={header}/>}
    <div className="body" children={children}/>
  </Wrapper>)
}
