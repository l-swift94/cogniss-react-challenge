import React, {useEffect, useState} from 'react';
import classnames from 'classnames'
import {authRequirements} from '../constants'
import styled from 'styled-components'
import PasswordMask from 'react-password-mask';
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import Control from './Control'
const Wrapper = styled(Control)`
  .form-control{
    width:100%;
    position:relative;
    padding:0;
    .password-input{
      width:100%;
      height: 100%;
      border: none;
      background: transparent;
    }
    .show-hide-button{
      position: absolute;
      right: 10px;
      top: 7px;
    }
  }
`

export default ({name, label, placeholder, className, required, form, tooltip}) => {
  const {errors} = form
  const [password, setPassword] = useState('')
  let error = errors[name]
  const inputClassName = classnames('form-control', {error})
  const pattern = /.(?=.*\d)(?=.*[a-zA_Z]).*/
  const minLength = authRequirements.PASSWORD_MIN
  const maxLength = authRequirements.PASSWORD_MAX
  const showButtonContent = <FontAwesomeIcon icon={['fal','eye']}/>
  const hideButtonContent = <FontAwesomeIcon icon={['fal','eye-slash']}/>
  const onChange = (e) =>{
    const password = e.target.value
    setPassword(password)
    form.setValue(name, password)
  }
  className = classnames(className, {error})
  useEffect(()=>{
    form.register({name}, {required, maxLength, minLength, pattern, tooltip})
  }, [name, form])
  return (<Wrapper className={className} label={label} name={name} error={error} patternLabel="Password must contain at least one number and one letter">
    <PasswordMask value={password} inputClassName="password-input"  buttonClassName="show-hide-button" id={name} className={inputClassName} name={name}
    useVendorStyles={false} onChange={onChange} maxLength={maxLength}
     showButtonContent={showButtonContent} hideButtonContent={hideButtonContent}/>
  </Wrapper>)
}
