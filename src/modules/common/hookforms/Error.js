import React from 'react';
export default ({error, label, patternLabel, min, max})=>{
  if(!error) return null
  let message = `${label} is not valid`
  if(error.type === 'pattern' && patternLabel) message = patternLabel
  if(error.type === 'required') message = `${label} is required`
  if(error.type === 'minLength') message = `${label} is too short`
  if(error.type === 'maxLength') message = `${label} is too long`
  if(error.type === 'min') message = `${label} must be ${min} or greater`
  if(error.type === 'max') message = `${label} must be ${max} or less`
  return <p className="alert alert-warning">{message}</p>
}
