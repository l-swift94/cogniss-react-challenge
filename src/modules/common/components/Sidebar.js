import React from 'react'
import styled from 'styled-components'
const View = styled.div`
  background: ${props => props.theme.colors.dark};
  font-size:18px;
  @media (min-width: 1024px) {
    float:left;
    display:flex;
    flex:0 0 ${props => props.theme.menu.width}px;
  }
  .menu-list {
    list-style:none;
    padding:0;
    margin:0;
    height:63px;
    display:flex;
    @media (min-width: 1024px) {
      height:auto;
      display:block;
    }
    li {
      // width: 20%;
      height:63px;
      display:block;
      float:left;
      flex-grow:1;
      text-align:center;
      @media (min-width: 1024px) {
        float:none;
        width:${props => props.theme.menu.width}px;
        text-align:left;
      }
      a, button{
        span{
          overflow:hidden;
          text-indent:-1000em;
          display:inline-block;
          width:0;
        }
        width: 100%;
        height:63px;
        color: ${props => props.theme.colors.lightGrey};
        text-transform:uppercase;
        display:flex;
        justify-content: center;
        align-items: center;
        position:relative;
        border-left:0 solid white;
        transition: 0.3s all;
        background-color: transparent;
        border: 0;
        outline: 0;
        @media (min-width: 1024px) {
          overflow:auto;
          text-indent:0;
          justify-content: left;
          span{
            overflow:auto;
            text-indent:0;
            width:auto;
          }
          .icon{
            margin-right:10px;
            width:35px;
          }
          padding-left: 20px;
          padding-right: 10px;
          background-position: 23px center;
          &:hover, &:focus, &.active {
            border-left:5px solid white;
          }
        }
        &:hover, &:focus, &.active {
          background-color: ${props => props.theme.colors.primary};
          color: ${props => props.theme.colors.white};
          text-decoration:none;
        }
      }
    }
    .submenu{
      padding:0;
      li{
        a,button{
          text-transform:none;
          font-size:16px;
        }
      }
    }
  }
`
class Sidebar extends React.Component {
  render() {
    return (
      <View className="sidebar">
        <ul className="menu-list">
          {this.props.children}
        </ul>
      </View>
    )
  }
}
export default Sidebar
