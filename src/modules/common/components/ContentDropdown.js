import React, { useState } from "react";
import styled from "styled-components";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {
  faInfoCircle,
  faChevronDown,
  faChevronUp
} from "@fortawesome/pro-regular-svg-icons";

const ContentDropdown = ({ heading, children, bgColor = "offWhite" }) => {
  const [dropdownState, setDropdownState] = useState(false);

  const Wrapper = styled.button`
    border: none;
    outline: none;
    width: 100%;
    border-radius: 12px;
    background-color: ${props => props.theme.colors[bgColor]};
    margin-bottom: 24px;

    .header-icon {
      margin-right: 10px;
      font-size: 20px;
    }
  `;

  const ChevronIcon = styled.div`
    outline: none;
    border: none;
    display: flex;
    align-items: center;
  `;

  const Header = styled.div`
    display: flex;
    justify-content: space-between;
  `;

  const Heading = styled.div`
    font-weight: 600;
    padding: 16px;
    display: flex;
    align-items: center;
  `;

  const Body = styled.div`
    padding: 0 16px 16px;
    text-align: left;
  `;

  return (
    <Wrapper
      onClick={() => {
        setDropdownState(!dropdownState);
      }}
    >
      <Header>
        <Heading>
          <FontAwesomeIcon icon={faInfoCircle} className="header-icon" />
          {heading}
        </Heading>
        <ChevronIcon>
          {dropdownState ? (
            <FontAwesomeIcon icon={faChevronUp} className="header-icon" />
          ) : (
            <FontAwesomeIcon icon={faChevronDown} className="header-icon" />
          )}
        </ChevronIcon>
      </Header>
      {dropdownState ? <Body>{children}</Body> : null}
    </Wrapper>
  );
};

export default ContentDropdown;
