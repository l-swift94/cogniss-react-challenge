import React, { useEffect } from "react";
import Modal from "react-modal";
Modal.setAppElement("body");

const PopoutModal = ({ modalState = false, styles, children, label }) => {
  useEffect(() => {
    modalState
      ? (document.body.style.overflow = "hidden")
      : (document.body.style.overflow = "scroll");
  }, []);

  return (
    <Modal isOpen={modalState} style={styles} contentLabel={label}>
      {children}
    </Modal>
  );
};

export default PopoutModal;
