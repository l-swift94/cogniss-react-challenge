import React, { useState } from "react";
import styled from "styled-components";

const ContentTabs = ({ tabs }) => {
  // Set the index of which tab is active. Default to first tab.
  const [tabActive, setTabActive] = useState(0);

  const Wrapper = styled.div`
    width: 100%;
    padding-bottom: 24px;

    .active-tab {
      border-left: 1px solid ${props => props.theme.colors.dark};
      border-top: 1px solid ${props => props.theme.colors.dark};
      border-right: 1px solid ${props => props.theme.colors.dark};
      border-bottom: 1px solid ${props => props.theme.colors.white};
      background-color: inherit;
      padding: 11px;
      position: relative;
    }

    .active-tab::after {
      position: absolute;
      content: "";
      bottom: -5px;
      left: 50%
      width: 100%
      height: 3px;
      background-color: ${props => props.theme.colors.white};
      transform: translate(-50%, -50%);
    }

    .active-tab-content {
      display: block;
    }
  `;

  const TabHeaders = styled.div`
    display: flex;
  `;

  const TabHeader = styled.button`
    background-color: background-color: ${props => props.theme.colors.offWhite};
    border: none;
    outline: none;
    padding: 12px;
    margin-right: 8px;
    font-weight: 500;
    font-size: 15px;
  `;

  const TabContent = styled.div`
    width: 100%;
    border: 1px solid ${props => props.theme.colors.dark};
    padding: 24px 16px;
    display: none;
    text-align: left;
  `;

  return (
    <Wrapper>
      <TabHeaders>
        {tabs.map(({ heading }, i) => {
          return (
            <TabHeader
              onClick={() => setTabActive(i)}
              key={heading + i}
              className={i === tabActive ? "active-tab" : null}
            >
              {heading}
            </TabHeader>
          );
        })}
      </TabHeaders>
      {tabs.map(({ content }, i) => {
        return (
          <TabContent
            key={i}
            className={i === tabActive ? "active-tab-content" : null}
          >
            {content}
          </TabContent>
        );
      })}
    </Wrapper>
  );
};

export default ContentTabs;
