import React from 'react'
import styled, { keyframes } from 'styled-components'
const rotate360 = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`
const LoadingWrap = styled.div`
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  position: fixed;
  background-color: ${props => props.theme.colors.dark};
  .loading-wrap {
    min-height: calc(100vh - 77px - 158px);
    position: relative;
      margin-left: -120px;
    & > * {
      position: absolute;
      opacity:0.5;
      top: 50%;
      left: 50%;
    }
    .spinner{
      border: 16px solid #f3f3f3; /* Light grey */
      border-top: 16px solid #C4C4C4; /* Blue */
      border-radius: 50%;
      width: 120px;
      height: 120px;
      animation: ${rotate360} 1s linear infinite;
    }
  }
`

const LoadingActivity = ({ className, style }) => {
  return (
    <LoadingWrap className={className} style={style}>
      <div className="loading-wrap">
        <div className="spinner" />
      </div>
    </LoadingWrap>
  )
}

export default LoadingActivity
