import React from "react";
import styled from "styled-components";

const GenericButton = ({ onClick, text, type, disabled = false }) => {
  const GenericButton =
    type === "inverse"
      ? styled.button`
          color: ${props => props.theme.colors.primary};
          background: ${props => props.theme.colors.white};
          padding: 8px 24px;
          border: 1px solid ${props => props.theme.colors.primary};
          min-width: 150px;
          border-radius: 30px;
          font-weight: 600;
          margin: 8px;

          &:hover {
            background: ${props => props.theme.colors.primary};
            color: ${props => props.theme.colors.white};
            transition: 0.3s;
          }

          &:disabled,
          &[disabled] {
            opacity: 0.6;
          }
        `
      : styled.button`
          color: ${props => props.theme.colors.white};
          background: ${props => props.theme.colors.primary};
          padding: 9px 25px;
          border: none;
          border-radius: 30px;
          min-width: 150px;
          font-weight: 600;
          margin: 8px;

          &:hover {
            transition: 0.3s;
            opacity: 0.6;
          }

          &:disabled,
          &[disabled] {
            opacity: 0.6;
          }
        `;

  return (
    <GenericButton
      onClick={onClick}
      aria-label={`${text} button`}
      disabled={disabled}
    >
      {text}
    </GenericButton>
  );
};

export default GenericButton;
