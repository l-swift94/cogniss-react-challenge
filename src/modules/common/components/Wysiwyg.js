import React, { Component } from 'react';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import { filterEditorState } from "draftjs-filters"
import { draftToMarkdown, markdownToDraft  } from 'markdown-draft-js';
import styled from 'styled-components'
const Wrapper = styled.div `
  max-width:735px;
  .wrapper{
    background-color: ${props => props.theme.colors.white};
    border: 1px solid ${props => props.theme.colors.grey};
    color:${props => props.theme.colors.black};
    .editor{
      padding:0 15px;
    }
  }
`;
export default class Wysiwyg extends Component {
  constructor(props) {
    super(props);
    this.state = {}
    let markdown = this.props.value
    if(!markdown){
      markdown = ''
    }
    const raw = markdownToDraft(markdown, {
      preserveNewlines : true
    });
    if (raw) {
      const contentState = convertFromRaw(raw);
      const editorState = EditorState.createWithContent(contentState);
      this.state = {
        editorState,
      };
    }
  }

  onEditorStateChange(nextState){

    let filteredState = nextState
    filteredState = filterEditorState(
      {
        blocks: [
          'paragraph',
          'header-one',
          'header-two',
          'header-three',
          'header-four',
          'header-five',
          'header-six',
          'unordered-list-item',
          'ordered-list-item',
        ],
        styles: ["BOLD", "ITALIC"],
        entities: [{
          type:'LINK',
          attributes:['url']
        }],
        maxNesting: 0,
        whitespacedCharacters: [],
      },
      filteredState,
    )
    this.setState({
      editorState:filteredState,
    })
    const raw = convertToRaw(filteredState.getCurrentContent())
    const markdown = draftToMarkdown(raw, {
      preserveNewlines: true
    })
    this.props.onChange(markdown)
  }

  render() {
    const { editorState } = this.state;
    const { toolbar } = this.props;
    const toolbarHidden = toolbar === false
    const toolbarOptions = {
      options: ['inline', 'blockType', 'list', 'link', 'remove', 'history'],
      inline: {
        options: ['bold', 'italic'],
      },
      blockType: {
        options: ['Normal', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6'],
      },
      list: {
        options: ['unordered', 'ordered'],
      },
    }
    return (
      <Wrapper>
        <Editor
          toolbarHidden={toolbarHidden}
          editorState={editorState}
          wrapperClassName="wrapper"
          editorClassName="editor"
          onEditorStateChange={e => this.onEditorStateChange(e)}
          toolbar={toolbarOptions}
        />
      </Wrapper>
    );
  }
}
