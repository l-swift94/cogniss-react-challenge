 const textLengths = {
  SHORT: 30,
  MEDIUM: 250,
  LONG: 5000,
  MAX: 35000
}
export {textLengths}
const authRequirements = {
  USERNAME_MIN:4,
  USERNAME_MAX:20,
  PASSWORD_MIN:8,
  PASSWORD_MAX:256,
  EMAIL_MAX:256,
}
export {authRequirements}
